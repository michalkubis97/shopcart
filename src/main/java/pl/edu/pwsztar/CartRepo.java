package pl.edu.pwsztar;

public interface CartRepo {
    Product getByName(String name);
    int getQuantityProduct(String name);
    int getFinalPrice();
    int getProductPrice();
}
