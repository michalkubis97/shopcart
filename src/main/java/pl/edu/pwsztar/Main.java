package pl.edu.pwsztar;

public class Main {

    public static void main( String[] argv ) {
        System.out.println("Shopping cart:");
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Carrot",3,2);
        shoppingCart.addProducts("Pineaplle",2,2);
        shoppingCart.addProducts("Roll",1,6);
        shoppingCart.addProducts("Carrot",3,2);
        System.out.println(shoppingCart.getProductsNames());
        shoppingCart.deleteProducts("Roll",1);
        System.out.println(shoppingCart.getProductsNames());

    }
}
