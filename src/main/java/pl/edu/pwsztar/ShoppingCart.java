package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.*;

public class ShoppingCart implements ShoppingCartOperation {

    final private static int MAX_QUANTITY = 100;
    Map<String,Product> products = new HashMap<String, Product>();

    public boolean addProducts(String productName, int price, int quantity) {
        if(quantity>MAX_QUANTITY){
            return false;
        }
        if(products.containsKey(productName)){
                        if(products.get(productName).getPrice() == price){
                                Product product = products.get(productName);
                                product.addQuantity(quantity); //
                                products.replace(product.getName(),product);
                                return true;
                            }
                    }else{
            return false;
        }
                products.put(productName, new Product(productName, price, quantity));
                return true;
    }

    public boolean deleteProducts(String productName, int quantity) {
        Product product = products.get(productName);
                if(Optional.ofNullable(product).isPresent()){
                        if(product.getQuantity() > quantity){
                               product.remove(quantity);
                                return true;
                            }else if(product.getQuantity() == quantity){
                                products.remove(productName);
                                return true;
                            }else{
                                return false;
                            }
                    }
        return false;
    }

    public int getQuantityOfProduct(String productName) {

        return 0;
    }

    public int getSumProductsPrices() {
        return 0;
    }

    public int getProductPrice(String productName) {

        return 0;
    }

    public List<String> getProductsNames() {
        List<String> names = new ArrayList<>();
        for(Map.Entry<String,Product> entry: products.entrySet()){
            names.add(entry.getValue().getName());
        }
        return names;
    }
}
