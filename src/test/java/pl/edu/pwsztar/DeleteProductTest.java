package pl.edu.pwsztar;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DeleteProductTest {

            @Test
    void deleteProductTest(){
                ShoppingCart shoppingCart = new ShoppingCart();

                Product mock1 = new Product("Butter",6,2);
                Product mock2 = new Product("Watermelon",7,1);
                Product mock3 = new Product("Fizzy drink",2,1);
                shoppingCart.addProducts(mock1.getName(),mock1.getPrice(),mock1.getQuantity());
                shoppingCart.addProducts(mock2.getName(),mock2.getPrice(),mock2.getQuantity());
                shoppingCart.addProducts(mock3.getName(),mock3.getPrice(),mock3.getQuantity());

                assertEquals(false, shoppingCart.deleteProducts(mock1.getName(),5));
                assertEquals(false, shoppingCart.deleteProducts(mock2.getName(),4));
                assertEquals(false, shoppingCart.deleteProducts("Tomato",4));
            }
}
