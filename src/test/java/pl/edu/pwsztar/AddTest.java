package pl.edu.pwsztar;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
public class AddTest {
    ShoppingCart shoppingCart = new ShoppingCart();
    @Test
    void test(){
        Product mock1 = new Product("Carrot",3,2);
        Product mock2 = new Product("Pineapple",2,2);
        Product mock3 = new Product("Carrot",1,6);
        assertEquals(true,shoppingCart.addProducts(mock1.getName(),mock1.getPrice(),mock1.getQuantity()));
        assertEquals(true,shoppingCart.addProducts(mock2.getName(),mock2.getPrice(),mock2.getQuantity()));
        assertEquals(false,shoppingCart.addProducts(mock3.getName(),mock3.getPrice(),mock3.getQuantity()));

    }
}
